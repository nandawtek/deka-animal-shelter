FROM node:14.17.6-alpine3.13

ENV RESTAPI_PORT 4050

WORKDIR /opt/app

COPY package*.json ./

# Node-GYP dependencies
RUN apk update
RUN apk add libc6-compat
RUN apk add alpine-sdk
RUN apk add py3-pip
RUN ln -s /lib/libc.musl-x86_64.so.1 /lib/ld-linux-x86-64.so.2

RUN npm install

COPY . .

EXPOSE ${RESTAPI_PORT}

CMD ["node", "."]