# Deka Animal Shelter

That project starts its live in a innovation week of deka software company.
It allows animal shetlers to add its pets and that ones appears on different social medias

## License

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Start APP

We use Docker to up mongo container

1. Install dependencies
2. Make `.env` file as a copy of `.env.example` file
3. Start mongo container
`docker run -p 27017:27017 --name animal-shelter-mongo -d mongo:latest`
4. Start RabbitMQ container
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.9-management`
5. Start app `npm start`

## Test APP

We use Docker to up mongo container

1. Install dependencies
2. Make `.env` file as a copy of `.env.example` file
3. Start mongo container
`docker run  -p 27017:27017 --name animal-shelter-mongo -d mongo:latest`
4. Start RabbitMQ container
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.9-management`
5. Start app `npm test`


## Docker run on production

1. Start mongo container
`docker run -p 27017:27017 --name animal-shelter-mongo -d mongo:latest`
2. Start RabbitMQ container
`docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.9-management`
3. Run your app version to deploy in production
`docker run --rm -e RESTAPI_PORT=4050 -e RESTAPI_VERSION=v1 -e NODE_ENV=production -e LOGGER_LEVEL=info -p 4050:4050 registry.gitlab.com/nandawtek/deka-animal-shelter:0.0.1 npm start`