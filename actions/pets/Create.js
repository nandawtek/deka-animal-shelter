const PetsService = require(`${process.cwd()}/services/pets/Service`)
const MessageBrokerService = require(`${process.cwd()}/services/messageBroker/Service`)
const AIService = require(`${process.cwd()}/services/ai/Service`)

class CreateAction {
  static async invoke(logger, repository, messageBroker, ai, pets) {
    pets = await AIService.checkImages(ai, pets)

    if (pets.succeed.length) {
      await PetsService.create(logger, repository, pets.succeed)
      MessageBrokerService.sendPetsMessage(logger, messageBroker, pets.succeed)
    }
    
    return { succeed: pets.succeed, failed: pets.failed }
  }
}

module.exports = CreateAction