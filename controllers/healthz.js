const SUCCESS = 200

async function healthz(req, res, next) {
  const response = {
    server: 'ok',
    port: process.env.RESTAPI_PORT,
  }
  res.status(SUCCESS).send(response)
}

module.exports = {
  healthz,
}
