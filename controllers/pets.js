const csv=require('csvtojson')
const SUCCESS = 200
const { logger } = require(`${process.cwd()}/infrastructure/logger`)
const petsRepository = require(`${process.cwd()}/infrastructure/repositories/PetsRepository`)
const Actions = require(`${process.cwd()}/actions/pets`)
const MessageBroker = require(`${process.cwd()}/infrastructure/RabbitMQ`)
const axios = require('axios')
const tf = require('@tensorflow/tfjs-node')
const nsfw = require('nsfwjs')
const ai = {
  axios,
  tf,
  nsfw
}

async function create(req, res, next) {
  const data = await readCSV(req)
  const queueName = 'deka-animal-shelter.pet.add_pet_on_pet_added'
  const messageBroker = await startMessageBroker(queueName)
  const { succeed, failed } = await Actions.create.invoke(logger, petsRepository, messageBroker, ai, data)

  res.status(SUCCESS).send({
    status: 'ok',
    succeed,
    failed
  })
}

async function startMessageBroker (queueName) {
  const messageBroker = new MessageBroker(queueName)

  return messageBroker
}

function readCSV(req) {
  const { files } = req.files
  const columsInLowerCaseFile = files.data.toString().replace(/^.*?\n/, s => s.toLowerCase())
  const data = csv()
    .fromString(columsInLowerCaseFile)

  return data
}

module.exports = {
  create,
}
