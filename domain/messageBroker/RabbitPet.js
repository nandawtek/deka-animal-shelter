const generateV4UUID = require(`${process.cwd()}/utils/generateUUID`)

class RabbitPet {
  constructor(pet, type, meta = {}) {
    this.id = generateV4UUID()
    this.type = type
    this.occurred_on = new Date().toISOString()
    this.attributes = pet
    this.meta = meta
  }

  toJsonApiStandard() {
    return {
      data: {
        id: this.id,
        type: this.type,
        occurred_on: this.occurred_on, 
        attributes: this.attributes,
        meta: this.meta
      }
    }
  }
}

module.exports = RabbitPet