class Pet {
  constructor (pet) {
    this._isValid(pet)
    this.title = pet.title
    this.description = pet.description
    this.image = pet.image
    this.contact = pet.contact
  }

  toMongoJSON () {
    return {
      title: this.title,
      description: this.description,
      image: this.image,
      contact: this.contact
    }
  }

  _isValid (pet) {
    if (!pet.title || !pet.description || !pet.image || !pet.contact) {
      throw new Error('You need to pass all fields filled in your CSV')
    }
  }
}

module.exports = Pet