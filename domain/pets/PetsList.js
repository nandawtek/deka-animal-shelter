const Pet = require('./Pet')
class PetsList {
  constructor (pets) {
    this.pets = pets.map(pet => new Pet(pet))
  }

  toMongoJSON() {
    return this.pets.map(pet => pet.toMongoJSON())
  }
}

module.exports = PetsList