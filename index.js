const express = require('express')
const Sentry = require('@sentry/node')
const fileUpload = require('express-fileupload')
const app = express()
const healthzRoutes = require('./routes/healthz')
const petsRoutes = require('./routes/pets')
const generateV4UUID = require('./utils/generateUUID')
const { logger } = require('./infrastructure/logger')

Sentry.init({
  dsn: 'https://glet_9e88636eb6dfad16f27f945afb8a417b@gitlab.com/api/v4/error_tracking/collector/31034632',
})
app.use(Sentry.Handlers.requestHandler())

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(fileUpload())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})
app.use((req, res, next) => {
  const body = req.body ? req.body : {}
  logger.info(`${req.method} - ${req.path}`, body)

  let traceRequestId = req.header('traceRequestId')
  if (!traceRequestId) {
    traceRequestId = generateV4UUID()
  }
  res.set('traceRequestId', traceRequestId)
  res.on('finish', () => {
    logger.info(
      `${req.method} - ${req.path} (${res.statusCode} - ${res.statusMessage})`
    )
  })
  next()
})
app.use(healthzRoutes)
app.use(petsRoutes)

app.use(Sentry.Handlers.errorHandler())
app.use(async (err, req, res, next) => {
  const BAD_REQUEST = 400
  const error = {}
  const uuid = generateV4UUID()
  error.errorId = uuid
  error.message = err.message
  error.code = err.statusCode || 500
  error.traceRequestId = res.get('traceRequestId')
  error.createdAt = new Date().toISOString()
  const response = {
    id: error.errorId,
    message: err.message,
    createdAt: error.createdAt,
  }
  error.response = response
  error.stack = err.stack
  logger.error(error.message, error)
  res.status(BAD_REQUEST).send(response)
  next()
})

module.exports = app
