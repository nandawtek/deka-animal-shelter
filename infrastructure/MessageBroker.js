const open = require('amqplib').connect('amqp://localhost')

class MessageBroker {
  sendMessage(params) {
    throw new Error(`Message Broker requires 'sendMessage' method`)
  }
}


module.exports = MessageBroker
