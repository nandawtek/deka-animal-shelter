const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient

let _db = null
const uri = process.env.MONGO_URI
const connection = () => {
  return MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(client => {
      _db = client.db()
      return _db
    })
}

const getDB = () => {
  if (_db) {
    return _db
  } else {
    return connection()
  }
}

exports.getDB = async () => {
  return getDB()
}
