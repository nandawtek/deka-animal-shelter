const open = require('amqplib').connect('amqp://localhost')
const MessageBroker = require('./MessageBroker')

class RabbitMQ extends MessageBroker {
  constructor (queue) {
    super()
    this.queue = queue
  }
  sendMessage(params) {
    open.then((connection) => {
      return connection.createChannel()
    }).then(async (channel) => {
      await channel.assertQueue(this.queue)
      return channel.sendToQueue(this.queue, Buffer.from(JSON.stringify(params)))
    }).catch(console.warn)
  }
}


module.exports = RabbitMQ
