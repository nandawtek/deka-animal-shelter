const NotImplementedError = require('../../utils/NotImplementedError')

module.exports = class Logger {
  /**
   * @param {any} message
   * @returns {Logger}
   */
  debug(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
  /**
   * @param {any} message
   * @returns {Logger}
   */
  info(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
  /**
   * @param {any} message
   * @returns {Logger}
   */
  warn(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
  /**
   * @param {any} message
   * @returns {Logger}
   */
  error(message, objectToLog = {}) {
    throw new NotImplementedError()
  }
}
