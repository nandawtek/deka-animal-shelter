const WinstonLogger = require('./WinstonLogger')

module.exports = class LoggerProvider {
  /** @type {import('./Logger') | null} */
  static #instance = null

  /** @returns {import('./Logger')} */
  static get logger() {
    if (this.#instance == null) {
      this.#instance = WinstonLogger.create()
    }
    return this.#instance
  }
}
