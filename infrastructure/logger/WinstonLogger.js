const winston = require('winston')
const Logger = require('./Logger')
const pkg = require(`${process.cwd()}/package.json`)

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  debug: 3,
}

const colors = {
  debug: 'blue',
  info: 'green',
  warn: 'yellow',
  error: 'red',
}
winston.addColors(colors)

module.exports = class WinstonLogger extends Logger {
  static create() {
    const logger = winston.createLogger({
      level: process.env.LOGGER_LEVEL,
      format: winston.format.json(),
      defaultMeta: {
        app: pkg.name,
        version: pkg.version,
      },
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()
          ),
          colorize: true,
        }),
      ],
      levels,
    })
    return new WinstonLogger(logger)
  }

  /** @type {winston.Logger} */
  #logger = null

  /** @param {winston.Logger} logger */
  constructor(logger) {
    super()
    if (logger == null) {
      throw new Error(
        `Winston logger implementation needs a non-null instance of a winston logger, nothing passed to the constructor instead.`
      )
    }
    this.#logger = logger
  }
  debug(message, objectToLog = {}) {
    this.#logger.debug(message, objectToLog)
    return this
  }
  info(message, objectToLog = {}) {
    this.#logger.info(message, objectToLog)
    return this
  }
  warn(message, objectToLog = {}) {
    this.#logger.warn(message, objectToLog)
    return this
  }
  error(message, objectToLog = {}) {
    this.#logger.error(message, objectToLog)
    return this
  }
}
