const LoggerProvider = require('./LoggerProvider')

module.exports = {
  logger: LoggerProvider.logger,
}
