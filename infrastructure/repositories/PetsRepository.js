const getDB = require(`${process.cwd()}/infrastructure/MongoDBConnection`).getDB
class PetsRepository {
  static async create(pets) {
    const db = await getDB()
    
    await db.collection('Pets')
      .insertMany(pets)
    
    return pets
  }
}

module.exports = PetsRepository