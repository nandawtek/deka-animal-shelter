require('../infrastructure/environment').loadEnvVars()
const app = require('../index')
const http = require('http')

const onError = (error) => {
  console.log(error.message)
}

const onListening = () => {
  const addr = server.address()
  console.log('Server is listening on port ' + addr.port)
}

app.set('port', process.env.RESTAPI_PORT)

const server = http.createServer(app)
server.listen(process.env.RESTAPI_PORT)
server.on('error', onError)
server.on('listening', onListening)
