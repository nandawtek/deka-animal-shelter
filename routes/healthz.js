const express = require('express')
const router = express.Router()
const Controllers = require(`${process.cwd()}/controllers/healthz`)
const { Try } = require(`${process.cwd()}/utils/Try`)

router.get(`/${process.env.RESTAPI_VERSION}/healthz`, Try(Controllers, 'healthz'))

module.exports = router
