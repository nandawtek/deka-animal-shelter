const express = require('express')
const router = express.Router()
const Controllers = require(`${process.cwd()}/controllers/pets`)
const { Try } = require(`${process.cwd()}/utils/Try`)

router.post(`/${process.env.RESTAPI_VERSION}/pets`, Try(Controllers, 'create'))

module.exports = router
