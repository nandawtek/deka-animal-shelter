class Service {
  static async checkImages(ai, pets) {
    let succeed = []
    let failed = []

    for (let i = 0; i < pets.length; i++) {
      const pet = pets[i]
      const { model, image } = await this._formatImage(ai, pet)
      const predictions = await this._getPredictions(model, image)
      image.dispose()
      const checkedPredictions = this._checkPredictions(predictions, succeed, failed, pet)
      succeed = checkedPredictions.succeed
      failed = checkedPredictions.failed
    }

    return { succeed, failed }
  }

  static async _formatImage(ai, pet) {
    const { axios, tf, nsfw } = ai
    const pic = await axios.get(pet.image, {
      responseType: 'arraybuffer',
    })
    const model = await nsfw.load()
    const image = await tf.node.decodeImage(pic.data, 3)

    return { model, image }
  }

  static async _getPredictions(model, image) {
    return model.classify(image)
  }

  static _checkPredictions(predictions, succeed, failed, pet) {
    predictions.forEach((prediction) => {
      if (prediction.className === 'Neutral') {
        pet.probability = prediction.probability
        if (prediction.probability >= 0.95) {
          succeed.push(pet)
        } else {
          failed.push(pet)
        }
      }
    })

    return { succeed, failed }
  }
}

module.exports = Service