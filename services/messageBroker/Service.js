const RabbitPet = require(`${process.cwd()}/domain/messageBroker/RabbitPet`)

class Service {
  static async sendPetsMessage(logger, messageBroker, pets) {
    logger.info('Pets to be sended to messageBroker', { pets })
    const type = 'deka.deka_animal_shelter.1.command.pet.pet_added'

    pets.forEach((pet) => {
      const petToRabbit = new RabbitPet(pet, type)
      messageBroker.sendMessage(petToRabbit.toJsonApiStandard())
    })
  }
}

module.exports = Service