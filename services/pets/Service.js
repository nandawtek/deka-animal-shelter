const PetsList = require(`${process.cwd()}/domain/pets/PetsList.js`)

class Service {
  static async create(logger, repository, pets) {
    logger.info('Pets to be stored', { pets })
    
    const petsList = new PetsList(pets)

    return repository.create(petsList.toMongoJSON())
  }
}

module.exports = Service