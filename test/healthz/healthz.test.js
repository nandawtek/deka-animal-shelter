require(`${process.cwd()}/infrastructure/environment`).loadEnvVars()
const Controllers = require(`${process.cwd()}/controllers/healthz`)
process.env.NODE_ENV = 'testing'
const request = require('supertest')
const app = require(`${process.cwd()}/index`)
const SUCCESS = 200

describe('Healthz endpoint', () => {
  test('with success', async () => {
    const response = await request(app).get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.server).toBe('ok')
    expect(response.body.port).toBe(process.env.RESTAPI_PORT)
  })
  it('with error', async () => {
    const BAD_REQUEST = 400
    const errorMessage = 'Your healthz server fails'
    jest.spyOn(Controllers, 'healthz').mockImplementation((req, res, next) => {
      throw new Error(errorMessage)
    })
    const response = await request(app)
      .get(`/${process.env.RESTAPI_VERSION}/healthz`)
    
    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.id).toBeTruthy()
    expect(response.header.tracerequestid).toBeTruthy()
    expect(response.body.message).toBe(errorMessage)
    expect(response.body.createdAt).toBeTruthy()
  })
})
