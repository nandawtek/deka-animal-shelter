require(`${process.cwd()}/infrastructure/environment`).loadEnvVars()
process.env.NODE_ENV = 'testing'
const request = require('supertest')
const PetsRepository = require(`${process.cwd()}/infrastructure/repositories/PetsRepository`)
const app = require(`${process.cwd()}/index`)
const SUCCESS = 200
const BAD_REQUEST = 400
const petsCSV = `${process.cwd()}/test/mocks/petsCSV.csv`
const wrongPetsCSV = `${process.cwd()}/test/mocks/wrongPetsCSV.csv`
const wrongPetImageCSV = `${process.cwd()}/test/mocks/wrongPetImageCSV.csv`

describe('Pets endpoints', () => {
  afterEach(() => {
    jest.restoreAllMocks()
  })

  test('can post CSV files', async () => {
    const response = await request(app)
      .post(`/${process.env.RESTAPI_VERSION}/pets`)
      .set('Content-Type', 'multipart/form-data')
      .attach('files', petsCSV) 
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('ok')
    expect(response.body.succeed.length).toBe(1)
    expect(response.body.succeed[0].contact).toBe('nacho@dekalabs.com')
    expect(response.body.succeed[0].probability).toBeGreaterThanOrEqual(0.95)
    expect(response.body.failed.length).toBe(1)
    expect(response.body.failed[0].contact).toBe('nacho@dekalabs.com')
    expect(response.body.failed[0].probability).toBeLessThan(0.95)
  })

  test('cannot post CSV files throws an error', async () => {
    const errorMessage = 'Database down'
    jest.spyOn(PetsRepository, 'create').mockImplementation((req, res, next) => {
      const error = new Error(errorMessage)
      return new Promise((resolve, reject) => reject(error))
    })

    const response = await request(app)
      .post(`/${process.env.RESTAPI_VERSION}/pets`)
      .set('Content-Type', 'multipart/form-data')
      .attach('files', petsCSV)

    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.createdAt).toBeTruthy()
    expect(response.body.id).toBeTruthy()
    expect(response.body.message).toBe(errorMessage)
  })

  test('post CSV files requires all fields filled', async () => {
    const errorMessage = 'You need to pass all fields filled in your CSV'
    const response = await request(app)
      .post(`/${process.env.RESTAPI_VERSION}/pets`)
      .set('Content-Type', 'multipart/form-data')
      .attach('files', wrongPetsCSV)

    expect(response.statusCode).toBe(BAD_REQUEST)
    expect(response.body.createdAt).toBeTruthy()
    expect(response.body.id).toBeTruthy()
    expect(response.body.message).toBe(errorMessage)
  })

  test('post CSV with wrong image', async () => {
    const response = await request(app)
      .post(`/${process.env.RESTAPI_VERSION}/pets`)
      .set('Content-Type', 'multipart/form-data')
      .attach('files', wrongPetImageCSV) 
    
    expect(response.statusCode).toBe(SUCCESS)
    expect(response.body.status).toBe('ok')
    expect(response.body.succeed.length).toBe(0)
    expect(response.body.failed.length).toBe(1)
    expect(response.body.failed[0].probability).toBeLessThan(0.95)
  })
})