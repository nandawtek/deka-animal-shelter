require(`${process.cwd()}/infrastructure/environment`).loadEnvVars()
process.env.NODE_ENV = 'testing'
const request = require('supertest')
const { logger } = require('../../infrastructure/logger/LoggerProvider')
const app = require(`${process.cwd()}/index`)
const petsCSV = `${process.cwd()}/test/mocks/petsCSV.csv`

const open = require('amqplib').connect('amqp://localhost')
const queueName = 'deka-animal-shelter.pet.add_pet_on_pet_added'
let connection

describe('RabbitMQ pets endpoints', () => {
  beforeAll(() => {
    connection = open
      .then((conn) => conn.createChannel())
      .then((channel) => {
        logger.warn('>>>>>>purguing queue')
        channel.purgeQueue(queueName)
        logger.warn('>>>>>>purged queue')
        return channel
      })
  })
  afterEach(() => {
    jest.restoreAllMocks()
  })

  test('RabbitMQ: can post CSV files', (done) => {
    connection
      .then((channel) => {
        logger.warn('>>>>>>connected queue to', channel)
        return channel.assertQueue(queueName).then(() => {
          logger.warn('>>>>>>asserted queue' + queueName)
          return channel.consume(queueName, (msg) => {
            logger.warn('>>>>>>queue message', msg)
            if (msg !== null) {
              const message = JSON.parse(msg.content.toString())
              expect(message.data.id).toBeTruthy()
              expect(message.data.type).toBe(
                'deka.deka_animal_shelter.1.command.pet.pet_added'
              )
              expect(message.data.occurred_on).toBeTruthy()
              expect(message.data.attributes.image).toBe(
                'https://live.staticflickr.com/3256/2594247316_b8a918ffa8_b.jpg'
              )
              expect(message.data.attributes.contact).toBe('nacho@dekalabs.com')
              expect(message.data.attributes.title).toBe('Magi is on fire')
              expect(message.data.attributes.description).toBe(
                'Magi is a fantastic dog, very kindly, she likes to walk with you all days'
              )
              expect(message.data.meta).toBeTruthy()
              channel.ack(msg)

              done()
            }
          })
        })
      })
      .catch(console.warn)

    request(app)
      .post(`/${process.env.RESTAPI_VERSION}/pets`)
      .set('Content-Type', 'multipart/form-data')
      .attach('files', petsCSV)
  })
})
