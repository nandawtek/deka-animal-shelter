const Try = (where, at) => async (req, res, next) => {
  try {
    return await where[at](req, res, next)
  } catch (err) {
    next(err)
  }
}

module.exports = {
  Try
}