const { v4: uuidv4 } = require('uuid')

const generateV4UUID = () => {
  return uuidv4()
}

module.exports = generateV4UUID
